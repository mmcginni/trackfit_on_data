import sys, re
import numpy as np
from scipy.odr import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if(len(sys.argv) != 3):
    print("Needs two arguments: fit number, and number of links per fit")


nlink_perfit = int(sys.argv[2])
print("number of links per fit is: " + str(nlink_perfit)) 

fhits = "./output_data/hits_" +sys.argv[1]+ ".dat"
print( "fname: {}".format(fhits))

xy_data = np.loadtxt(fhits, usecols=[0])
z_data = np.loadtxt(fhits, usecols=[1])

# read in the fit parameters
ffit = "./output_data/fit_" +sys.argv[1]+ ".dat"
#regex = re.compile("(\d+\.*\d*|\d*\.\d+)((e|E)(\+|\-)(\d+))*")
regex = re.compile("((\+|\-)\d+\.*\d*|\d*\.\d+)((e|E)(\+|\-)(\d+))*")
with open( ffit ) as f:
    line = f.readline()
    params = regex.findall(line)
    dXdS = params[0][0]
    X0 = params[1][0]
    line = f.readline()
    params = regex.findall(line)
    dYdS = params[0][0]
    Y0 = params[1][0]    


print ("dXdS: {}, X0: {}".format(dXdS,X0))
print ("dYdS: {}, Y0: {}".format(dYdS,Y0))

fig = plt.figure(figsize=(24,8))
ax = fig.add_subplot(131, projection='3d')
#ax.scatter(x_data,y_data,z_data)

# draw the best fit
z = np.linspace(0,100,1000)
x = float(dXdS)*z + float(X0)
y = float(dYdS)*z + float(Y0)
ax.plot(x,y,z)
 
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")

ax.set_xlim([-100,100])
ax.set_ylim([-100,100])
ax.set_zlim([0,100])

#x-z and y-z projections
ax1 = fig.add_subplot(132)
ax1.scatter(xy_data[0:nlink_perfit], z_data[0:nlink_perfit])
ax1.plot(x,z)
ax1.set_xlabel("x")
ax1.set_ylabel("z")
ax1.set_xlim([-3,3])
ax1.set_ylim([0,100])

ax2 = fig.add_subplot(133)
ax2.scatter(xy_data[nlink_perfit:], z_data[nlink_perfit:])
ax2.plot(y,z)
ax2.set_xlabel("y")
ax2.set_ylabel("z")
ax2.set_xlim([-3,3])
ax2.set_ylim([0,100])

plt.savefig("./img/line_LLS_fit"  + sys.argv[1] + ".png")
