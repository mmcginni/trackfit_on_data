Running linear least squares regression trackfit. Works for a general run.

# IMPORANT FILES

constants.hh holds constants for the fitter, right now just uncertainties in x, y, u and v.


get_hit_data.cc (in input_data) takes a root file and outputs a file with superid, bx, link, and absolute coordinates of candidate events for fitting. Coordinates are in cm. Candidate events are bxs with hits in all modules (links). The diffrent possible tracks which are formed through permutating the hits in the modules. Works for a general number of links, but if you know the number of links, set nlinks equal to that number in L106 to decrease runtime. Currently requires links to be sequential.


main_LLSfit.cc  takes file with hits from a track fitter or from get_hit_data. If there are N links, each set of N lines are to be fit together. In this way, can have two fits which share a hit if the hit data is repeated. All fits must have N hits. Three arguments: name of file, number of fits to be done, and module orientation. The orientation is the axis which the 2S module measures precisely, ie, across the strips. Possible orientations: x, y, u, v. Needs one letter per module, for example xyuvxy is the setup in the MUonE station.


LLS.py plots the hits and the fit in 3d, and in the x-z and y-z projections.


Uses eigen 3.3.9 and matplotlib 1.2.0.


# INSTRUCTIONS

get hit data (root file not inlcuded since it is too large):
```
cd input_data
g++ -o get_hit_data.exe get_hit_data.cc $(root-config --cflags --libs)
./get_hit_data.exe run_0044.root run44_trackcands.dat
```

main_LLS:
```
make
./bin/main_LLSfit_line.exe input_data/run44_trackcands.dat 10000 xyxy
```

LLS.py:
```
python python/LLS.py 7000
```
