#include <iostream>
#include <fstream>
#include <limits>
#include <stdlib.h>

#include "TFile.h"
#include "TTree.h"

#include <vector>
#include <cmath>

using namespace std;


//------------------------------------------------------------------------//
//Inputs a ttree with bx, link, absolute coordinates, outputs a file
//with superid, bx, link, and absolute coordinates of candidate events
//for fitting. Coordinates are in cm.
//
//Candidate events are bxs with hits in all modules (links). The diffrent
//possible tracks which are formed through permutating the hits in the
//modules are printed, so that each nlink lines form a single candidate
//track.
//
//Works for a general number of links, but if you know the number of links,
//set nlinks equal to that number in L106 to decrease runtime.
//------------------------------------------------------------------------//



//------------------------------------------------------------------------//
struct Hit{
  //------------------------------------------------------------------------//
  UInt_t superid;
  int  bx, link;
  float coordX, coordY, coordZ;
  //float uncertX, uncertY;
};


//------------------------------------------------------------------------//
int get_nlink(string filename, int* nlink, int* maxlink){
//------------------------------------------------------------------------//

  TFile *file = TFile::Open(filename.c_str());
  auto tree = file->Get<TTree>("Cereal");

  int link, address;
  float bend;
  vector<int> linkvec;
  vector<int>::iterator iter;
  tree->SetBranchAddress("Link", &link);
  tree->SetBranchAddress("Address", &address);
  tree->SetBranchAddress("Bend", &bend);

  for (int i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);

    if ((address == 0) || (address == 255) || (bend == 7)) {
      //unphysical values
    }
    else if (find(linkvec.begin(), linkvec.end(), link) == linkvec.end()){
      linkvec.push_back(link);
    }

    if (i > 10000){
      break;
    }
    
  }

  int nmiss = 0;
  for (int j = 0; j < linkvec.size(); j++){
    if (find(linkvec.begin(), linkvec.end(), link) == linkvec.end()){
      nmiss++;
    }
  }

  
  *nlink = linkvec.size();
  *maxlink = linkvec.size() + nmiss;
};


//------------------------------------------------------------------------//
void permute_hits(int nlink, vector<Hit> bx_by_link[],
		  vector<Hit>* permuted_bx){
//------------------------------------------------------------------------//
//implemented as an odometer.
  vector<Hit>::iterator itvec[nlink];
  for (int linkit = 0; linkit != nlink; linkit++){
    vector<Hit>::iterator it = bx_by_link[linkit].begin();
    itvec[linkit] = it;
  }
  while (itvec[0] != bx_by_link[0].end()){
    for ( int j = 0; j != nlink; j++){
      //cout<<j<<endl;
      vector<Hit> link = bx_by_link[j];
      vector<Hit>::iterator it = itvec[j];
      Hit h = *it;
      permuted_bx->push_back(h);
    }
    ++itvec[nlink-1];
    for (int i = nlink-1; (i>0) && (itvec[i] == bx_by_link[i].end()); --i){
      itvec[i] = bx_by_link[i].begin();
      ++itvec[i-1];
    }

  }
};



//------------------------------------------------------------------------//
int main(int argc, char ** argv){
  if (argc != 3){
    cerr << "needs two arguments: input file and output files." << endl;
    return -1;
  }


  string inputname = argv[1];

  int nlink, maxlink;
  get_nlink(inputname, &nlink, &maxlink);
  cout<<"nlink: "<<nlink<<endl;

  TFile *inputFile = TFile::Open(inputname.c_str());
  auto tree = inputFile->Get<TTree>("Cereal");

  string outputname = argv[2];
  ofstream outputFile;
  outputFile.open(outputname, ios::trunc);

  UInt_t superid;
  int bx, link, address;
  float absX, absY, absZ, localX, localY, bend;
  //float cpi_4 = cos(M_PI_4);
  //float spi_4 = sin(M_PI_4);
  float trig_pi_4 = sqrt(2)/2;

  vector<Hit> hitvec;
  vector<Hit>::iterator iter;
  vector<Hit> bx_by_link[maxlink];

  tree->SetBranchAddress("SuperID", &superid);
  tree->SetBranchAddress("Bx", &bx);
  tree->SetBranchAddress("AbsX", &absX);
  tree->SetBranchAddress("AbsY", &absY);
  tree->SetBranchAddress("AbsZ", &absZ);
  tree->SetBranchAddress("LocalX", &localX);
  tree->SetBranchAddress("LocalY", &localY);
  tree->SetBranchAddress("Link", &link);
  tree->SetBranchAddress("Address", &address);
  tree->SetBranchAddress("Bend", &bend);

  vector<int> bxvec;
  vector<int> linkvec;
  UInt_t lastsid;
  int lastbx;

  int tothit = 0;
  int link_dup = 0;
  int unphys = 0;
 
  vector<int> entryvec;
  vector<int> nhitvec;

  //Loops through tree to find candidate events, which have
  //hits in each module/link. 
  //The last entry and number of entires for the canditate
  //events are added to entryvec and nhitvec.
  cout << "total hits: " << tree->GetEntries() << endl;
  for (int i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    //cout<<i<<endl;
    //cout<<"link: "<<link<<" "<<"bx: "<<bx<<endl;
    if (i%1000000 == 0){
      cout<<i<<endl;
    }
   
    if ((address == 0) || (address == 255) || (bend == 7)) {
      unphys++;
      //unphysical values
    }
    else if (i == 0){
      lastsid = superid;
      lastbx = bx;
      linkvec.push_back(link);
    }
    else if ( (superid == lastsid) && (bx == lastbx) ){
      if (find(linkvec.begin(), linkvec.end(), link) == linkvec.end()){
	linkvec.push_back(link);
      }
      else {
	link_dup++;
      }
    }
    else{
      if (linkvec.size() == nlink){
	entryvec.push_back(i);
	nhitvec.push_back(linkvec.size() + link_dup + unphys);

	tothit = tothit + linkvec.size() +link_dup;
      }

      linkvec.clear();
      linkvec.push_back(link);
      lastsid = superid;
      lastbx = bx;
      link_dup = 0;
      unphys = 0;
      
      if (entryvec.size() > 100){
	break;
      }
      
    }
  }


  //For each candidate event, hit structs are formed and
  //each track candidate is found via permuting the hits
  //then added to hitvec.
  for (int i = 0; i < entryvec.size(); i++){
    int entry = entryvec[i];
    int nhit = nhitvec[i];
    for (int j = entry - nhit; j < entry; j++){
      tree->GetEntry(j);
      
      if ((address == 0) || (address == 255) || (bend == 7)) {
	//unphysical values
      }
      else{
	Hit h;
	
	h.superid = superid;
	h.bx = bx;
	h.link = link;
	h.coordX = absX;
	h.coordY = absY;
	h.coordZ = absZ;
      
	if (link == 2){
	  cout<<"link2"<<endl;
	  cout<<" localX: "<<localX<<endl;
	  cout<<" absX: "<<absX<<endl;
	  cout<<" localY: "<<localY<<endl;
	  cout<<" absY: "<<absY<<endl;

	  h.coordX = (localX - localY)*trig_pi_4;
	  h.coordY = (localX + localY)*trig_pi_4;
	}
	else if (link == 3){
	  h.coordX = (localX + localY)*trig_pi_4;
	  h.coordY = (-localX + localY)*trig_pi_4;
	  cout<<"link3"<<endl;
	  cout<<" localX: "<<localX<<endl;
	  cout<<" absX: "<<absX<<endl;
	  cout<<" localY: "<<localY<<endl;
	  cout<<" absY: "<<absY<<endl;

	}
	bx_by_link[h.link].push_back(h);
      }
    }

    permute_hits(nlink, bx_by_link, &hitvec);
    for (int i = 0; i < nlink; i++){
      bx_by_link[i].clear();
    }

  }

  //writing hit info to file from hitvec.
  for (iter = hitvec.begin(); iter < hitvec.end(); ++iter){
    outputFile << iter->superid << " " << iter->bx << " " << iter->link
	       << " " << iter->coordX << " " << iter->coordY << " "
	       << iter->coordZ << "\n";
  }

  outputFile.close();

  cout<<"number of total hits: "<<tothit<<endl;
  cout<<"number of total bx: "<<entryvec.size()<<endl;
  cout<<"number of total fits: "<<hitvec.size()/nlink<<endl;
}
