CXX := g++
CXXFLAGS := -o $(shell root-config --cflags) -Iinclude -I ./inlcude/eigen/
LDFLAGS := $(shell root-config --libs)

all : bin/main_LLSfit_line.exe

bin/main_LLSfit_line.exe : ./obj/LLS_solver.o ./obj/get_nlink.o
bin/main_LLSfit_line.exe : OBJFILES = ./obj/LLS_solver.o ./obj/get_nlink.o

bin/%.exe :: src/%.cc $(OBJFILES)
	$(CXX) $(CXXFLAGS) $< -o $@ $(LDFLAGS) $(OBJFILES)

obj/%.o : src/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean :
	rm bin/*.exe
	rm obj/*.o
	rm -f src/*~
	rm -f include/*~

