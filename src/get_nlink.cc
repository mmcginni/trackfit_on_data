#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "TTree.h"

#include <vector>
#include <string>

using namespace std;

//-----------------------------------------------------------------------//
//Finds number of links used in this run.
//
//
//-----------------------------------------------------------------------//


//------------------------------------------------------------------------//
int get_nlink(string inputname)
//------------------------------------------------------------------------//
{

  ifstream inputfile;
  inputfile.open(inputname, ios::in);


  UInt_t lastsuperid;
  int lastbx;
  vector<int> linkvec;

  string line;
  while (getline(inputfile, line)){

    UInt_t superid;
    int bx;
    int link;

    size_t pos;
    for (int j = 0; j < 3;j++){
      pos = line.find(' ');
      string word = line.substr(0,pos);
      line = line.substr(pos+1);

      if (pos == string::npos){
	cerr << "input file in wrong format" << endl;
	return -1;
      }
      
      else if (j == 0){
	superid = stoi(word);
      }
      else if (j == 1){
	bx = stoi(word);
      }
      else if (j == 2){
	link = stoi(word);
      }
    }

    if (linkvec.size() == 0){
      lastsuperid = superid;
      lastbx = bx;
      linkvec.push_back(link);
    }
    else if ( (superid == lastsuperid) && (bx == lastbx) ){
      if (find(linkvec.begin(), linkvec.end(), link) == linkvec.end()){
	  linkvec.push_back(link);
	}
    }
    else{  
      cout<<"Number of links: "<<linkvec.size()<<endl;
      return linkvec.size();

    }
     
  }

}
