#include <iostream>
#include <limits>
#include <stdlib.h>

// Eigen and add-on
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;



//  ------------------------------------------------------------------------
//
//  Without multiple scattering the fits in x & y are independent. See, eg :
//   https://towardsdatascience.com/qr-matrix-factorization-15bae43a6b2
//   https://johnwlambert.github.io/least-squares/
//   https://www.cs.cornell.edu/~bindel/class/cs3220-s12/notes/lec10.pdf
//  for the connection between QR decomposition and LS
//
//  ------------------------------------------------------------------------
void LLSsolver(VectorXd m, VectorXd z, VectorXd* sigma_est, VectorXd* params, 
	       MatrixXd* covarianceP, VectorXd* deltas,
	       VectorXd* pulls, double* chisq, VectorXd* denoms) 
//  ------------------------------------------------------------------------
{
  
  //number of measurements to set size of Jacobian, etc.
  const int n_meas = m.rows();


  //
  // Explicit jacobian for the linear case
  //     
  MatrixXd F  = MatrixXd(n_meas,2);
  for( int i=0; i<n_meas; i++ ) { 
    F(i,0) = z(i);
    F(i,1) = 1;
  }
    
  //
  // Covaraince matrix and inverse
  //
  MatrixXd G  = MatrixXd(n_meas,n_meas);
  G = G.Zero(n_meas,n_meas);
  MatrixXd Gsqrt  = MatrixXd(n_meas,n_meas);
  Gsqrt = Gsqrt.Zero(n_meas,n_meas);
  MatrixXd V  = MatrixXd(n_meas,n_meas);
  V = V.Zero(n_meas,n_meas);
  for( int i=0; i<n_meas; i++ ) { 
    Gsqrt(i,i) = 1./(*sigma_est)(i);
    G(i,i) = ( 1./(*sigma_est)(i) )*( 1./(*sigma_est)(i) );
    V(i,i) = (*sigma_est)(i)*(*sigma_est)(i);
  }  


  //
  // G is diagonal, so we can scale m & F with G^1/2
  //
  MatrixXd Fscaled  = MatrixXd(n_meas,2);
  VectorXd mscaled = VectorXd(n_meas);
  Fscaled = Gsqrt * F;
  mscaled = Gsqrt * m;


  //
  // Q,R decomposition of the scaled Jacobian 
  //
  MatrixXd Q = Fscaled.colPivHouseholderQr().householderQ().setLength(Fscaled.colPivHouseholderQr().nonzeroPivots()) ;
  MatrixXd thinQ(MatrixXd::Identity(n_meas,2));
  Q = Q * thinQ;
  MatrixXd QT = MatrixXd(n_meas,n_meas);
  QT = Q.transpose();

  MatrixXd R = Fscaled.colPivHouseholderQr().matrixR().topLeftCorner(2, 2).template triangularView<Upper>();
  MatrixXd RI = MatrixXd(2,2);
  RI = R.inverse();


  //
  // solve for best fit parameters
  //
  VectorXd p(2,1);
  p = RI * QT * mscaled;
  *params = p;

    
  //    
  // get the parameter and residual covariance
  //
  MatrixXd covP(2,2);
  covP = (F.transpose() * G * F).inverse();
  *covarianceP = covP;
  MatrixXd  covR(n_meas,n_meas);
  covR = V  - (F)*(covP)*(F.transpose());
    
    
  //
  // standardized residuals
  //
  VectorXd rsd(n_meas);
  for( int i=0; i<n_meas; i++ ) {
    rsd(i) = m(i) - (F*p)(i);
    (*deltas)(i) = rsd(i);
    (*denoms)(i) = sqrt(covR(i,i));
    (*pulls)(i) = rsd(i)/sqrt(covR(i,i));

    /*    
    cout<<i<<endl;
    cout<<rsd(i)<<endl;
    cout<<rsd(i)/sqrt(covR(i,i))<<endl;
    cout<<sqrt(covR(i,i))<<endl;
    */
  }

  // chisq
  *chisq = rsd.transpose() * G * rsd;
}
