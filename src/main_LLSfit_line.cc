#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "TH1F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TStyle.h"

#include <vector>
#include <string>
#include <eigen/Eigen/Dense>

#include "get_nlink.hh"
#include "LLS_solver.hh"
#include "constants.hh"

using namespace std;
using namespace Eigen;

//-----------------------------------------------------------------------//
//Takes file with hits from a track fitter. If there are N links, each 
//set of N lines are to be fit together. In this way, can have two fits 
//which share a hit if the hit data is repeated. All fits must have N hits.
//
//Works for a general run.
//
//Three arguments: name of file, number of fits to be done, and module 
//orientation. The orientation is the axis which the 2S module measures
//precisely, ie, across the strips. Possible orientations: x, y, u, v. Needs
//one letter per module, for example xyuvxy is the setup in the MUonE
//station.
//
//File has: SuperID, bx, link, absX, absY, absZ for each hit.
//-----------------------------------------------------------------------//



//----------------------------------------------------------------------//
struct Hit{
//----------------------------------------------------------------------//
  UInt_t superid;
  int bx, link;
  float x, y, z;

  bool operator< (const Hit &other) const{
    return link < other.link;
  }
};



//------------------------------------------------------------------------//
int main(int argc, char ** argv){
  if (argc != 4){
    cerr << "argument is file with track candidates, number of fits, and module orientation." << endl;
    return -1;
  }

  string inputname = argv[1];
  int totFit = stoi(argv[2]);
  string orient = argv[3];

  ifstream inputfile;
  inputfile.open(inputname, ios::in);

  int nFit = 0;
  //int nlink = get_nlink(inputname);
  int nlink = orient.length();
  int nlink_perfit = 3;
  int ndof = nlink_perfit - 2;  //since the fit is linear, lines have 2 dof
  //ndof = ndof + 5;

  VectorXd pX(2,1),pY(2,1);
  VectorXd deltaX(nlink_perfit,1),deltaY(nlink_perfit,1);
  VectorXd denomX(nlink_perfit,1), denomY(nlink_perfit,1);
  VectorXd pullX(nlink_perfit,1),pullY(nlink_perfit,1);
  MatrixXd covpX(2,2),covpY(2,2);

  VectorXd uncertX(nlink_perfit,1),uncertY(nlink_perfit,1);
  for (int i = 0; i < nlink_perfit; i++){
    
    if ((orient[i] == 'x') || (orient[i] == 'y')){
      uncertX(i) = sigmaX;
      uncertY(i) = sigmaX;
    }
    else if ((orient[i] == 'u') || (orient[i] == 'v')){
      uncertX(i) = sigmaUV;
      uncertY(i) = sigmaUV;
    }
    else {
      cerr << "Possible orientations: x, y, u, v." << endl;
      return -1;
    }

  }

  //double sigmaX = 10.0, sigmaY = 5.0;

  double chisqX, chisqY;
  char fname[256];

  TH1F * hdeltax = new TH1F( "hdeltax","hdeltax",100,-0.05,0.05);
  TH1F * hdeltay = new TH1F( "hdeltay","hdeltay",100,-0.05,0.05);
  TH1F * hdenomx = new TH1F( "hdenomx","hdenomx",100,-0.01,0.01);
  TH1F * hdenomy = new TH1F( "hdenomy","hdenomy",100,-0.01,0.01);
  TH1F * hpullx = new TH1F( "hpullx","hpullx",100,-6,6);
  TH1F * hpully = new TH1F( "hpully","hpully",100,-6,6);
  TH1F * hchisqx = new TH1F( "hchisqx","hchisqx",60,0,6);
  TH1F * hchisqy = new TH1F( "hchisqy","hchisqy",60,0,6);

  //plotting uv measurements
  TH1F * hdeltauv = new TH1F( "hdeltauv","hdeltauv",100,-0.05,0.05);
  TH1F * hdenomuv = new TH1F( "hdenomuv","hdenomuv",100,-0.01,0.01);
  TH1F * hpulluv = new TH1F( "hpulluv","hpulluv",100,-6,6);
  TH1F * hdeltauvx = new TH1F( "hdeltauvx","hdeltauvx",100,-0.05,0.05);
  TH1F * hpulluvx = new TH1F( "hpulluvx","hpulluvx",100,-6,6);
  TH1F * hdeltauvy = new TH1F( "hdeltauvy","hdeltauvy",100,-0.05,0.05);
  TH1F * hpulluvy = new TH1F( "hpulluvy","hpulluvy",100,-6,6);


  vector<Hit> bunchvec;

  //Read input file line-by-line and form into Hit structs.
  //Call LLSsolver to fit the track candidates, formed by 
  //nlink number of lines.
  string line;
  while (getline(inputfile, line)){
    Hit h;

    size_t pos;
    for (int j = 0; j < 5;j++){
      pos = line.find(' ');
      string word = line.substr(0,pos);
      line = line.substr(pos+1);

      if (pos == string::npos){
	cerr << "input file in wrong format" << endl;
	return -1;
      }
      
      else if (j == 0){
	h.superid = stoi(word);
      }
      else if (j == 1){
	h.bx = stoi(word);
      }
      else if (j == 2){
	h.link = stoi(word);
      }
      else if (j == 3){
	h.x = stof(word);
      }
      else if (j == 4){
	h.y = stof(word);
	h.z = stof(line);
      }
    }
    
    bunchvec.push_back(h);
    
    if (bunchvec.size() == nlink){

      sort(bunchvec.begin(), bunchvec.end());
      
      if (nFit > totFit){
	      break;
      }
     
      int i_x = 0;
      int i_y = 0;

      VectorXd x = VectorXd(nlink_perfit);
      VectorXd y = VectorXd(nlink_perfit);
      VectorXd z_x = VectorXd(nlink_perfit);
      VectorXd z_y = VectorXd(nlink_perfit);
      for (int i = 0; i < nlink; i++){
	if (orient[i] == 'x'){
	  x(i_x) = bunchvec[i].x;
	  z_x(i_x) = bunchvec[i].z;
	  i_x++;
	}
	else if (orient[i] == 'y'){
	  y(i_y) = bunchvec[i].y;
	  z_y(i_y) = bunchvec[i].z;
	  i_y++;
	}
	else if ((orient[i] == 'u') || (orient[i] == 'v')){
	  x(i_x) = bunchvec[i].x;
	  y(i_y) = bunchvec[i].y;
	  z_x(i_x) = bunchvec[i].z;
	  z_y(i_y) = bunchvec[i].z;
	  i_x++;
	  i_y++;
	}
	else {
	  cerr << "Possible orientations: x, y, u, v." << endl;
	  return -1;
	}
      }     

      if (x.rows() != z_x.rows()){
	cerr<<"x and z_x not the same size"<<endl;
	}
      if (y.rows() != z_y.rows()){
	cerr<<"y and z_y not the same size"<<endl;
	}
      if (z_x.rows() != z_y.rows()){
	cerr<<"z_x and z_y not the same size"<<endl;
	}

      //perform linear fits in x & y independently.
      LLSsolver(x, z_x, &uncertX, &pX, &covpX, &deltaX, &pullX, &chisqX, &denomX);
      LLSsolver(y, z_y, &uncertY, &pY, &covpY, &deltaY, &pullY, &chisqY, &denomY);

      /*
      //printing fit params and covariances to check HLS
      cout<<"pX"<<endl;
      for (int i=0; i<2; i++){
	cout<<pX(i)<<endl;
      }

      cout<<"covpX"<<endl;
      for (int i=0; i<2; i++){
	for (int j=0; j<2; j++){
	  cout<<covpX(i,j)<<endl;
	}
      }

      cout<<"pY"<<endl;
      for (int i=0; i<2; i++){
	cout<<pY(i)<<endl;
      }

      cout<<"covpY"<<endl;
      for (int i=0; i<2; i++){
	for (int j=0; j<2; j++){
	  cout<<covpY(i,j)<<endl;
	}
      }     
      */ 

      nFit++;
      // dump generated data for every 1k-th track 
      if(!(nFit%1000) ) { 
	sprintf(fname, "./output_data/hits_%d.dat",nFit);
	FILE * fh_hits = fopen(fname,"w");
	for( int i=0; i<nlink_perfit; i++ ) { 
	  fprintf(fh_hits,"%lf\t%lf\t\n", 
		  x(i),z_x(i));
	}
	for( int i=0; i<nlink_perfit; i++ ) { 
	  fprintf(fh_hits,"%lf\t%lf\t\n", 
		  y(i),z_y(i));
	}
	fclose(fh_hits);
      }
        
      // dump best-fit parameters for every 1k-th track
      if(!(nFit%1000) ) {
	sprintf(fname, "./output_data/fit_%d.dat",nFit);
	FILE * fh_fit = fopen(fname,"w");
	fprintf(fh_fit,"X: %lf\t%lf\n", pX(0),pX(1));
	fprintf(fh_fit,"Y: %lf\t%lf\n", pY(0),pY(1));
	fclose(fh_fit);
      }
      
      //assumes three links per station for each fit, so that second link
      //per station per fit is the uv modules combined.
      // histogram pulls, deltas, and chisq
      for( int i=0; i<nlink_perfit; i++ ) {

	hdeltax->Fill(deltaX(i));
	hdeltay->Fill(deltaY(i));
	hdenomx->Fill(denomX(i));
	hdenomy->Fill(denomY(i));
	hpullx->Fill(pullX(i));
	hpully->Fill(pullY(i));

	if (i%3 == 1){
	  hdeltauv->Fill(deltaX(i));
	  hdenomuv->Fill(denomX(i));
	  hpulluv->Fill(pullX(i));
	  hdeltauv->Fill(deltaY(i));
	  hdenomuv->Fill(denomY(i));
	  hpulluv->Fill(pullY(i));

	  hdeltauvx->Fill(deltaX(i));
	  hpulluvx->Fill(pullX(i));
	  hdeltauvy->Fill(deltaY(i));
	  hpulluvy->Fill(pullY(i));
	}

      }
      hchisqx->Fill(chisqX);
      hchisqy->Fill(chisqY);
      
      if (nFit > totFit-1){
	break;
      }
      
      bunchvec.clear();
    }
    
  }
  
  // Plot
  TCanvas * c = new TCanvas("c","c",0,0,600,500);

  TH1F* framedx = (TH1F*)(c->DrawFrame(-0.05,0,0.05,1.1*hdeltax->GetMaximum()));
  framedx->GetXaxis()->SetTitle("delta X");
  hdeltax->Draw("sames");
  hdeltax->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_deltaX.png");

  TH1F* framedy = (TH1F*)(c->DrawFrame(-0.05,0,0.05,1.1*hdeltay->GetMaximum()));
  framedy->GetXaxis()->SetTitle("delta Y");
  hdeltay->Draw("sames");
  hdeltay->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_deltaY.png");

  TH1F* framedenx = (TH1F*)(c->DrawFrame(-0.01,0,0.01,1.1*hdenomx->GetMaximum()));
  framedenx->GetXaxis()->SetTitle("pull denom X");
  hdenomx->Draw("sames");
  hdenomx->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_denomX.png");

  TH1F* framedeny = (TH1F*)(c->DrawFrame(-0.01,0,0.01,1.1*hdenomy->GetMaximum()));
  framedeny->GetXaxis()->SetTitle("pull denom Y");
  hdenomy->Draw("sames");
  hdenomy->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_denomY.png");

  TH1F* framedenuv = (TH1F*)(c->DrawFrame(-0.01,0,0.01,1.1*hdenomuv->GetMaximum()));
  framedenuv->GetXaxis()->SetTitle("pull denom UV");
  hdenomuv->Draw("sames");
  hdenomuv->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_denomUV.png");

  TH1F* framex = (TH1F*)(c->DrawFrame(-6,0,6,1.1*hpullx->GetMaximum()));
  framex->GetXaxis()->SetTitle("pull X");
  hpullx->Draw("sames");
  hpullx->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_pullX.png");

  TH1F* framey = (TH1F*)(c->DrawFrame(-6,0,6,1.1*hpully->GetMaximum()));
  framey->GetXaxis()->SetTitle("pull Y");
  hpully->Draw("sames");
  hpully->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_pullY.png");


  string dof_str = "chi2_" + to_string(ndof) + "dofF";
  char* chi2_title = const_cast<char*>(dof_str.c_str());

  string plot_str = "ROOT::Math::chisquared_pdf(x," +
    to_string(ndof) + ",0)";
  char* toplot =  const_cast<char*>(plot_str.c_str());

  TF1 chi2_ndofF(chi2_title,toplot,0,30);
  chi2_ndofF.SetLineColor(kRed);

  hchisqx->Scale(1./(hchisqx->Integral()*hchisqx->GetBinWidth(1)));
  TH1F* framecx = (TH1F*)(c->DrawFrame(0,0,6,1.1*hchisqx->GetMaximum()));
  framecx->GetXaxis()->SetTitle("#chi^{2} X");
  hchisqx->Draw("samesh");
  chi2_ndofF.Draw("same");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_chisqX.png");

  hchisqy->Scale(1./(hchisqy->Integral()*hchisqy->GetBinWidth(1)));
  TH1F* framecy = (TH1F*)(c->DrawFrame(0,0,6,1.1*hchisqy->GetMaximum()));
  framecy->GetXaxis()->SetTitle("#chi^{2} Y");
  hchisqy->Draw("samesh");
  chi2_ndofF.Draw("same");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_chisqY.png");

  //plotting uv deltas and pulls
  TH1F* frameduv = (TH1F*)(c->DrawFrame(-0.05,0,0.05,1.1*hdeltauv->GetMaximum()));
  frameduv->GetXaxis()->SetTitle("delta UV");
  hdeltauv->Draw("sames");
  hdeltauv->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_deltauv.png");

  TH1F* frameuv = (TH1F*)(c->DrawFrame(-6,0,6,1.1*hpulluv->GetMaximum()));
  frameuv->GetXaxis()->SetTitle("pull UV");
  hpulluv->Draw("sames");
  hpulluv->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_pulluv.png");


  TH1F* frameduvx = (TH1F*)(c->DrawFrame(-0.05,0,0.05,1.1*hdeltauvx->GetMaximum()));
  frameduvx->GetXaxis()->SetTitle("delta UV x");
  hdeltauvx->Draw("sames");
  hdeltauvx->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_deltauvx.png");

  TH1F* frameuvx = (TH1F*)(c->DrawFrame(-6,0,6,1.1*hpulluvx->GetMaximum()));
  frameuvx->GetXaxis()->SetTitle("pull UV x");
  hpulluvx->Draw("sames");
  hpulluvx->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_pulluvx.png");


  TH1F* frameduvy = (TH1F*)(c->DrawFrame(-0.05,0,0.05,1.1*hdeltauvy->GetMaximum()));
  frameduvy->GetXaxis()->SetTitle("delta UV y");
  hdeltauvy->Draw("sames");
  hdeltauvy->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_deltauvy.png");

  TH1F* frameuvy = (TH1F*)(c->DrawFrame(-6,0,6,1.1*hpulluvy->GetMaximum()));
  frameuvy->GetXaxis()->SetTitle("pull UV y");
  hpulluvy->Draw("sames");
  hpulluvy->Fit("gaus");
  gStyle->SetOptStat("emrou");
  c->Print("img/line_LLS_pulluvy.png");
}
