//------------------------------------------------------------------------//
//Constants for fitter, such as measurement uncertainties
//
//------------------------------------------------------------------------//

const float trig_pi_4 = sqrt(2)/2;

//measurement uncertainties in in cm
const float inflationX = 1.7; //inflate uncertainty, due to alignment, etc.
const float sigmaX = 0.0026*inflationX;
const float sigmaY = 1.4;

//since u module rotation about z is pi/4, X and Y uncertainties are the same
//since v module roation is -pi/4, u and v uncertainties are the same
//const float varUV = (sigmaX*sigmaX + sigmaY*sigmaY)*trig_pi_4*trig_pi_4;
//const float sigmaUV = sqrt(varUV);


//for data from David: hits_unaligned, combining fine measurements of uv modules
const float inflationUV = 1; //inflate uncertainty due to combining z
const float sigmaUV = sigmaX*inflationUV; 
