#ifndef LLS_SOLVER
#define LLS_SOLVER

#include <eigen3/Eigen/Dense>
//#include <EigenRand/EigenRand>

using namespace std;
using namespace Eigen;

void LLSsolver(VectorXd m, VectorXd z, VectorXd* sigma_est, VectorXd* params, 
	       MatrixXd* covarianceP, VectorXd* deltas, VectorXd* pulls,
	       double* chisq, VectorXd* denoms); 

#endif
