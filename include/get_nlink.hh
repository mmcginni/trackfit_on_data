#ifndef GET_NLINK
#define GET_NLINK

#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "TTree.h"

#include <vector>
#include <string>

using namespace std;


int get_nlink(string inputname);

#endif
